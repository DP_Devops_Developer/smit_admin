from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),

    path('players/', views.players, name='players'),
    path('players/<int:pid>', views.player_by_id, name='idplayers'),
    path('banned-players/', views.banned_players, name='banned_players'),
    path('privillage-player/', views.privillaged_players, name='star_player'),


    path('player/<int:pid>/lobbyhistory/', views.userLobbyHistory, name="userlobbyhistory"),
    path('lobby/<int:lid>/lobbyhistory/', views.lobbyHistory, name="lobbyhistory"),
    path('banplayer/', views.banUser, name='banplayer'),
    path('activate/player/', views.activate_player, name='activateplayer'),

    path('lobbies/', views.lobbies, name='lobbies'),
    path('lobby_structure/<int:aid>/admin', views.lb_structure, name='lbstructure'),
    path('banned/lobby_structure/<int:aid>/admin', views.all_banned_lbs, name='bndlbstructure'),

    path('update/lbstuct', views.update_lb_structures, name='updatelbstruct'),
    path('delete/lbstuct', views.delete_lb_structures, name='deletelbstruct'),
    path('update-active/lbstuct', views.update_active_structures, name='upactlbstruct'),
    

    path('admins/', views.adminData, name='admins'),
    path('banned-admins/', views.banned_admins, name='banned_admins'),
    path('modify-admin/', views.modifyAdmin, name='modadmin'),
    path('delete-admin/', views.deleteAdmin, name='deleteadmin'),


    path('commisisons/', views.commissions, name='commissions'),
 

    path('variations/', views.variations, name='variations'),
    path('delete/variations/', views.delete_variation, name='delvariation'),
    # path('lobby-structures/', views.clubs, name='lb_structures'),

    path('achievement', views.achievements, name='achievement'),
    path('update-achievement', views.updateAch, name='updateach'),
    path('delete-achievement', views.deleteAchievement, name='deleteach'),

    path('leisure/', views.leisure, name='leisure'),
    path('lobby/<int:lid>/leisure/', views.leisurePlayer, name='leisureplayer'),

    path('withdrawals', views.withdrawals, name='withdrawals'),
    path('logout', views.logut,name='logout'),
    path('login',views.login, name='login'),

    path('regenerate/session', views.create_new_session, name='regenerate')
]