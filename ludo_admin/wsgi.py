"""
WSGI config for ludo_admin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/howto/deployment/wsgi/
"""

# import newrelic.agent
# from django.conf import settings
# newrelic.agent.initialize(settings.NEW_RELIC_CONFIG_FILE)

from django.core.wsgi import get_wsgi_application
import os

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ludo_admin.settings')
application = get_wsgi_application()
# application = newrelic.agent.WSGIApplicationWrapper(application)
